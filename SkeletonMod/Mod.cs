namespace SkeletonMod {
	public class Mod : Godasum.modding.Mod
	{

		public override string MODID { get { return "skeletonmod"; } }

		public override void setup() {
			this.register_entity<ships.SkeletonShip>("skeleton_ship");

		}

	}
}
