namespace SkeletonMod.ships {


	public class SkeletonShip : Godasum.objects.battle.ShipBO
	{

		public override string entity_id { get { return "skeleton_ship"; } }

		public override string scene_path { get { return "res://SkeletonMod/ships/SkeletonShip.tscn"; } }


	}
}
