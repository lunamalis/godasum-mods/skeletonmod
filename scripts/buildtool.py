#!/usr/bin/env python3.7
"""A build tool to be used for exporting mods for Godasum.

NOTICE: This build tool, like Godasum, is under an early alpha.
It may be changed.

The latest version may be seen on the Gitlab repository for the Skeleton mod.
While you are of course free to change whatever you'd like, it is a good idea
to pay attention to changes that occur in the default version.
"""
# The version for Godasum this was built for.
VERSION = "0.0.1"
# If you modify this build tool at all, modify these flavor values.
FLAVOR = "Vanilla"
# The version of this flavor.
FLAVOR_VERSION = "0.0.1"
from typing import *
from typing import Callable
import os
import sys
import platform
import json
import subprocess
import shutil
import distutils.dir_util
import tarfile
import zipfile


class Buildtool:

	PATHS: Dict[str, str] = {}

	actions: Dict[str, Tuple[Callable, str]]

	_settings: Optional[Dict[str, str]] = None

	_modinfo: Optional[Dict[str, str]] = None

	def __init__(self):
		self.actions = {
			"help": (self._action_help, "Get info on an action."),
			"build": (self._action_build_debug, "Build your mod and copy it to your mods folder under debug mode."),
			"build-release": (self._action_build_release, "Build your mod and copy it to your mods folder under release mode."),
			"export-debug": (self._action_export_debug, "Export the mod under Debug. Also calls `build_dll`."),
			"export-release": (self._action_export_release, "Export the mod under Release."),
			"build-dll": (self._action_build_dll, "Build DLL. This will always be in Debug mode."),
			"update-dll": (self._action_update_dll, "Update DLL. This will always be in Debug mode. Will copy DLLs to the mods folder."),
			"package": (self._action_package, "Package your build into a mod for Godasum."),
			"copy-package": (self._action_copy_package_to_mods, "Copy your packaged mod into Godasum's mods directory."),
			"zip": (self._action_zip, "Zip the mod into a .zip file."),
			"clean": (self._action_clean, "Clean up, and delete your build and .mono directories."),
			"clean-mod": (self._action_clean_mod, "Clean up, deleting your mod in your mods folder.")
		}

		self.PATHS["ABSOLUTE_DIR"] = os.path.dirname(__file__)
		self.PATHS["REPO_DIR"] = os.path.dirname(self.PATHS["ABSOLUTE_DIR"])
		self.PATHS["PATH_SETTINGS"] = os.path.join(self.PATHS["REPO_DIR"], "buildtool.paths.json")

	def start(self):
		print(f"Starting Godasum Modding Buildtool, version {VERSION}.")
		print(f"Flavor {FLAVOR}-{FLAVOR_VERSION}")

		actions: List[str] = sys.argv[1:]
		if len(actions) < 1:
			print("Actions:")
			for act in self.actions:
				print(f"\t{act}")
			return

		self.handle_actions(actions)

		print("Finished.")

	def handle_actions(self, actions):
		if actions[0] == "help":
			# Only do help, ignore the rest.
			self.actions["help"][0]()
			return

		for act in actions:
			if act not in self.actions:
				print(f"Unknown action `{act}`.")
				return

			if act == "help":
				print("Invalid combination: help.")
				return

		for i in range(len(actions)):
			act = actions[i]

			print(f"({i + 1}/{len(actions)}) Starting action `{act}`.")
			self.actions[act][0]()

		print("Finished actions.")

	# <actions>

	def _action_help(self):
		"""Get help on an action."""
		if len(sys.argv) != 3:
			print("buildtool help <action name>")
			return
		other: str = sys.argv[2]
		if other not in self.actions:
			print(f"Can not help with non-existant action `{other}`.")

		print(f"Info:\n\t{other}:\n\t\t{self.actions[other][1]}")

	def _action_build_dll(self):
		self._load_settings()
		self._load_modinfo()

		proc = subprocess.run(
			[
				self._settings["godot_exe_path"],
				"--path",
				os.path.join(self.PATHS["REPO_DIR"], "project.godot"),
				"--no-window",
				"--quit",
				"--build-solutions"
			],
			stdout=sys.stdout,
			stderr=sys.stderr,
			text=True
		)
		if proc.returncode != 0:
			print("Error while building DLLs.")
			exit()

	def _action_update_dll(self):
		self._load_settings()
		self._load_modinfo()

		self._action_build_dll()
		# Then copy it to the mods dir.
		src_mono_dir: str = os.path.join(self.PATHS["REPO_DIR"], ".mono", "temp", "bin", "Debug")
		dest_mono_dir: str = os.path.join(self._settings["mods_dir"], self._get_mod_path(), "mono")
		src_files: List[str] = os.listdir(src_mono_dir)
		for filename in src_files:
			shutil.copy2(
				os.path.join(src_mono_dir, filename),
				os.path.join(dest_mono_dir, filename)
			)

	def _action_export_debug(self):
		"""Export mod under debug mode."""
		# Maybe a bad way of doing this?
		# We export for release, then build debug DLLs.
		# Then we copy the debug DLLs over the release ones.
		# Why?
		# Godot doesn't support exporting PCK files in debug mode
		# from the command line.
		self._load_modinfo()
		print("Running release build first...")
		self._build()
		print("Building DLLs in debug mode...")
		self._action_build_dll()

		print("Overwriting release DLLs with debug DLLs...")
		debug_bin_dir: str = os.path.join(self.PATHS["REPO_DIR"], ".mono", "temp", "bin", "Debug")
		dest_dir: str = os.path.join(self.PATHS["REPO_DIR"], "build", f"data_{self._modinfo['cname']}", "Assemblies")
		src_files: List[str] = os.listdir(debug_bin_dir)
		for filename in src_files:
			# Only replace files, don't copy them if it doesn't exist there.
			if os.path.isfile(os.path.join(dest_dir, filename)):
				shutil.copy2(
					os.path.join(debug_bin_dir, filename),
					os.path.join(dest_dir, filename)
				)

	def _action_export_release(self):
		self._build()

	def _action_package(self):
		"""Package the mod into a folder that works with Godasum."""
		self._load_modinfo()

		package_dir: str = os.path.join(self.PATHS["REPO_DIR"], "build", self._get_mod_path())

		mono_dir: str = os.path.join(self.PATHS["REPO_DIR"], "build", f"data_{self._modinfo['cname']}", "Assemblies")
		zip_path: str = os.path.join(self.PATHS["REPO_DIR"], "build", "data.zip")

		os.makedirs(package_dir, exist_ok=True)

		# Copy the zip in first.
		shutil.copy2(zip_path, package_dir)

		# Then copy all the DLLs.
		os.makedirs(os.path.join(package_dir, "mono"), exist_ok=True)
		src_files: List[str] = os.listdir(mono_dir)
		for filename in src_files:
			shutil.copy2(
				os.path.join(mono_dir, filename),
				os.path.join(package_dir, "mono", filename)
			)

		# Then copy the modinfo
		shutil.copy2(os.path.join(self.PATHS["REPO_DIR"], "modinfo.json"), package_dir)

	def _action_copy_package_to_mods(self):
		"""Copy the game's .zip file to the mods directory."""
		self._load_modinfo()
		self._load_settings()

		distutils.dir_util.copy_tree(
			os.path.join(self.PATHS["REPO_DIR"], "build", self._get_mod_path()),
			os.path.join(self._settings["mods_dir"], self._get_mod_path())
		)

	def _action_clean(self):
		"""Clean up."""
		dirs: Set[str] = {
			os.path.join(self.PATHS["REPO_DIR"], "build"),
			os.path.join(self.PATHS["REPO_DIR"], ".mono")
		}

		for directory in dirs:
			if os.path.islink(directory):
				print(f"Directory {directory} is actually a symlink.")
				print("Will not delete symlinks.")
				return
			if os.path.isfile(directory):
				print(f"Directory {directory} is actually a file.")
				print("Will not delete files, only directories.")
				return
			if os.path.ismount(directory):
				print(f"Directory {directory} is actually a mount.")
				print("Will not delete mounts.")
				return
			if not os.path.isdir(directory):
				print(f"Directory {directory} either doesn't exist or is not a directory.")
				print("Skipping.")
				continue

			print(f"Deleting {directory}")
			shutil.rmtree(directory, False)

	def _action_clean_mod(self):
		"""Clean up mods directory."""
		self._load_settings()
		self._load_modinfo()
		delete: str = os.path.join(self._settings["mods_dir"], self._get_mod_path())

		if os.path.islink(delete):
			print(f"Directory {delete} is actually a symlink.")
			print("Will not delete symlinks.")
			return
		if os.path.isfile(delete):
			print(f"Directory {delete} is actually a file.")
			print("Will not delete files, only directories.")
			return
		if os.path.ismount(delete):
			print(f"Directory {delete} is actually a mount.")
			print("Will not delete mounts.")
			return
		if not os.path.isdir(delete):
			print(f"Directory {delete} either doesn't exist or is not a directory.")
			return

		print(f"Deleting {delete}")
		shutil.rmtree(delete, False)

	def _action_zip(self):
		"""Zip a package directory into a .zip file."""
		self._load_settings()
		self._load_modinfo()
		zippable_path: str = os.path.join(self.PATHS["REPO_DIR"], "build", self._get_mod_path())

		with zipfile.ZipFile(f"{zippable_path}.zip", "w", zipfile.ZIP_DEFLATED) as zf:
			for root, dirs, files in os.walk(zippable_path):
				for file in files:
					zf.write(
						os.path.join(root, file),
						os.path.relpath(os.path.join(root, file), os.path.join(zippable_path))
					)

		# Godasum doesn't support tar.gz files yet. If/When it does, we can use this instead for smaller mod files.
		# with tarfile.open(f"{zippable_path}.tgz", "w:gz") as tf:
		# 	tf.add(zippable_path, arcname=os.path.basename(zippable_path))

	def _action_build_debug(self):
		print("Exporting as debug...")
		self._action_export_debug()
		print("Packaging...")
		self._action_package()
		print("Copying to mods folder...")
		self._action_copy_package_to_mods()

	def _action_build_release(self):
		print("Exporting as release...")
		self._action_export_release()
		print("Packaging...")
		self._action_package()
		print("Copying to mods folder...")
		self._action_copy_package_to_mods()

	# </actions>

	def _load_settings(self):
		if self._settings is not None:
			return

		# Load up our settings.
		if os.path.exists(self.PATHS["PATH_SETTINGS"]):
			if os.path.isfile(self.PATHS["PATH_SETTINGS"]):
				# Read it up.
				with open(self.PATHS["PATH_SETTINGS"], 'r') as f:
					self._settings = json.loads(f.read())
			else:
				raise FileNotFoundError("Settings path exists, but is not a file.")
		else:
			print("Please enter a path to your Godot executable.")
			godot_exe_path: str
			while True:
				answer: str = input("> ")
				if not os.path.exists(answer):
					print("That doesn't exist.")
					continue

				if not os.path.isfile(answer):
					print("That isn't a file.")
					continue

				godot_exe_path = answer
				break

			print("Please enter a path to your mods directory.")
			mods_dir: str
			while True:
				answer: str = input("> ")
				if not os.path.exists(answer):
					print("That doesn't exist.")
					continue

				if not os.path.isdir(answer):
					print("That isn't a directory.")
					continue

				mods_dir = answer
				break

			self._settings = {
				"godot_exe_path": godot_exe_path,
				"mods_dir": mods_dir
			}
			text: str = json.dumps(self._settings, indent="\t")
			with open(self.PATHS["PATH_SETTINGS"], 'x') as f:
				f.write(text)

	def _load_modinfo(self):
		"""Read the mod's mod info."""
		if self._modinfo is not None:
			return self._modinfo

		with open(os.path.join(self.PATHS["REPO_DIR"], "modinfo.json"), 'r') as f:
			self._modinfo = json.loads(f.read())

	def _build(self):
		self._load_settings()
		self._load_modinfo()

		# We need to export.
		proc = subprocess.run(
			[
				self._settings["godot_exe_path"],
				"--path",
				os.path.join(self.PATHS["REPO_DIR"], "project.godot"),
				"--no-window",
				"--quit",
				# https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_for_dedicated_servers.html
				"--export-pack",
				"export_mod",
				os.path.join(self.PATHS["REPO_DIR"], "build", "data.zip")
			],
			stdout=sys.stdout,
			stderr=sys.stderr,
			text=True
		)
		if proc.returncode != 0:
			print("Error while building.")
			exit()

	def _ask_bool(self, default: bool = True) -> bool:
		if default:
			ask_string = "[Y/n]> "
		else:
			ask_string = "[y/N]> "

		while True:
			answer: str = input(ask_string).strip().lower()
			if answer == "":
				return default
			elif answer in {"y", "ye", "yes", "true", "1", "sure"}:
				return True
			elif answer in {"n", "no", "false", "0", "noo"}:
				return False

	def _get_mod_path(self) -> str:
		if self._modinfo is None:
			raise ValueError("Mod info not loaded; can't find zip path.")
		return f"{self._modinfo['cname']}-{self._modinfo['loader_version']}-{self._modinfo['version']}"


def main():
	b = Buildtool()
	try:
		b.start()
	except KeyboardInterrupt:
		print("\nKeyboardInterrupt: Bye")


if __name__ == "__main__":
	main()
