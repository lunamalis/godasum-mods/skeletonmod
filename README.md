# SkeletonMod

This is a skeleton mod for Godasum. It contains very little content, and is
meant to be used as a template.

This mod uses a sprite from the [200 CC0 Spaceship Sprites collection by Wisedawn](https://opengameart.org/content/200-cc0-spaceship-sprites).

## How To Build

You will need to run the build tool. See the [instructions](notes/BuildToolInstructions.md).
