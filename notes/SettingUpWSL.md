# Setting Up on Windows Subsystem Layer

This will show you how to set up a headless runner with WSL.

This will assume you are using WSL Ubuntu 18.04.

This will assume your project directory is on the Windows side, rather than the
Linux side.

Run these commands, which are taken from [here](https://www.mono-project.com/download/stable/#download-lin).

This will download and install Mono so you can build your project solution.

```bash
sudo apt install gnupg ca-certificates
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
sudo apt update

# This may take several minutes, even after the download.
# This will let you actually compile your project.
sudo apt install mono-devel  # ~150MB download.
# Optionally, you may want to install this, which will get you debugging
# symbols for errors.
sudo apt install mono-dbg  # ~5800KB download.
```

Next, you will need to install Godot, headless edition.

In all the following commands, if you are not using Godot version 3.2.2, replace
all instances of 3.2.2 with your version.

You will have to choose a folder to install in.

```bash
# You must choose a directory to install it in.
# It doesn't really matter where it is.
mkdir -p ~/path/to/your/godot

# Recommended to cd to a temporary directory.
cd /tmp

wget http://downloads.tuxfamily.org/godotengine/3.2.2/mono/Godot_v3.2.2-stable_mono_linux_headless_64.zip
unzip Godot_v3.2.2-stable_mono_linux_headless_64.zip

mv GodotSharp ~/path/to/your/godot/GodotSharp
mv Godot_v3.2.2-stable_mono_linux_headless.64 ~/path/to/your/godot/.

# Optional, frees space. You don't need this file anymore.
rm Godot_v3.2.2-stable_mono_linux_headless_64.zip
```

Next, you will need to download the export templates. This is a large,
seemingly unavoidable download, unfortunately, and it is usually around
500MB.

```bash
# Recommended to cd to a temporary directory, again.
cd /tmp

# Download the templates
wget http://downloads.tuxfamily.org/godotengine/3.2.2/mono/Godot_v3.2.2-stable_mono_export_templates.tpz
unzip Godot_v3.2.2-stable_mono_export_templates.tpz  # Creates a folder named `templates`

# You need to move it to this directory specifically.
# It doesn't matter where you installed Godot; it uses this folder.
mkdir -p ~/.local/share/godot/templates/3.2.2.stable.mono
mv templates/* ~/.local/share/godot/templates/3.2.2.stable.mono

# Optional, frees up space. You don't need this anymore.
rm Godot_v3.2.2-stable_mono_export_templates.tpz
```

Now, you can run the Build Tool in WSL. For example, you could run:

```bash
./scripts/buildtool.bash build
# or
python3.7 scripts/buildtool.py build
# ^^^^^^^ Needs at least Python 3.7.
```

For other instructions, refer to the build tool instructions.

## Errors

You may see a few errors while doing this.

```bash
System.Exception: Failed to build project
  at GodotTools.Export.ExportPlugin._ExportBeginImpl (System.String[] features, System.Boolean isDebug, System.String path, System.Int32 flags) [...] in <...>:0
  at GodotTools.Export.ExportPlugin._ExportBegin (System.String[] features, System.Boolean isDebug, System.String path, System.Int32 flags) [...] in <...>:0
```

This possibly means you haven't installed Mono correctly. Review the Mono installation instructions above.

```bash
ERROR: get_language_code: Invalid locale 'C'.
    At: core/translation.cpp:945.
ERROR: set_locale: Unsupported locale 'C', falling back to 'en'.
    At: core/translation.cpp:969.
```

This doesn't seem to cause any problems.

```bash
WARNING: _notification: Scan thread aborted...
    At: editor/editor_file_system.cpp:1129.
```

I have no idea what this error means.

However, it seems that you can just re-build and it will work. We need to figure out why this happens
before we can easily use CI.
