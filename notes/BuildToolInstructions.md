# Build Tool Instructions

The build tools have two purposes:

* Setting up your development environment.
* Building your mod.

Running the build tools is easy. Open a command prompt, and run the appropriate
file.

## Running

### Windows

On Windows, you can open Powershell by pressing WINDOWS+X and clicking on
`Windows Powershell`.

Change to the directory your mod is in:

```powershell
cd C:\path\to\your\mod
```

Then, you can run the build tools like this:

```powershell
.\scripts\buildtool.bat
```

### *nix

On Unix/Linux systems, `cd` to your mod directory. Simply call the bash script:

```bash
./scripts/buildtool.bash
```

### Windows Subsystem Layer

See the setting up for WSL guide.

## Setting Up

When you first create a mod, you will have to set up your development environment.

Your mod will need Godasum's DLL file to compile. First, you have to download Godasum,
and copy the `Godasum.dll` to the location:

`/path/to/your/mod/lib/Godasum/Godasum.dll`

You will have to make the folders `lib/Godasum` yourself.
